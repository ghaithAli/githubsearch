<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BaseMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //create the results table, this will contain the result we fetched from the GitHub API
        Schema::create('github_results', function (Blueprint $table) {
            $table->increments('id');
            $table->string('repository_name');
            $table->string('owner_name');
            $table->string('file_name');
            $table->string('repository_url');
            $table->string('provider');
            $table->mediumText('search_text');
            $table->double('rating');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('github_results');
    }
}
