<!-- login.blade.php -->

@extends('layouts.main')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Login with GitHub</div>
                    <p class="loginText">
                        Hello User, In order to search code on all the repositories in Github, we are requiring to be authenticated.
                        Here we are using a github Oauth to get token and use it to do the search query, please authorize the app to use
                        GitHub account to make the search query (see image below).
                    </p>
                    <img class="githubScreenshot" src="{{url("img/github.png")}}">

                    <div class="panel-body">
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <a href='{{url('/redirect')}}' class="btn btn-success">Authorize with GitHub</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection