@extends('layouts.main')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Welcome to GitHub search, in order to use this tool, you must log in with your
                GitHub account.</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <a href="{{url("/login")}}">Go to login</a>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
