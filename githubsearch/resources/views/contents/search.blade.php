@extends('layouts.main')

@section('content')
    <div class="col-xs-6 col-xs-offset-3">
        <P class="text-center">Type code to search github!</P>
        <form class="form-horizontal" role="form" method="POST" action="{{ url('/search') }}">
            {{ csrf_field() }}

            <div class="form-group">
                <div class="col-xs-12">
                    <input id="code_text" type="text" class="form-control" name="code_text" required autofocus placeholder="code..">
                </div>
            </div>

            <div class="form-group">
                <div class="col-xs-6 col-xs-offset-3">
                    <div class="col-xs-12">
                        <label class="">Number of hits</label>

                    </div>
                    <div class="col-xs-12">
                        <select class="form-control" id="results_size" name="results_size" >
                            <option selected="selected"  value="25">25</option>
                            <option value="10">10</option>
                            <option value="15">15</option>
                            <option value="20">20</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="col-xs-6 col-xs-offset-3">
                    <div class="col-xs-12">
                        <label class="">Sort type</label>

                    </div>
                    <div class="col-xs-12">
                        <select class="form-control" id="sort_type" name="sort_type" >
                            <option selected="selected"  value="rating">By score</option>
                            <option value="owner_name">By owner name</option>
                            <option value="repository_name">By repository name</option>
                            <option value="file_name">By file name</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group hidden">
                <input type="text" name="access_token" value="{{$user->token}}">
            </div>

            <div class="form-group">
                <div class="col-xs-12">
                    <button type="submit" class="btn btn-success pull-right">
                        Search
                    </button>
                </div>
            </div>
        </form>
    </div>

@stop()
