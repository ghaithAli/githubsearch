@extends('layouts.main')

@section('content')

    <div class="col-xs-12">
        <h2>Results</h2>
    </div>
    <div class="col-xs-12">
        @if(sizeof($result)>0)
            @foreach($result as $item)
                <div class="col-xs-12 col-md-6 col-lg-4 well mainContainer">
                    <p><strong>Repository Name</strong></p>
                    <p>@isset($item->repository_name){{$item->repository_name}}@endisset</p>

                    <p><strong>Owner</strong></p>
                    <p>@isset($item->owner_name){{$item->owner_name}}@endisset</p>

                    <p><strong>File name</strong></p>
                    <p>@isset($item->file_name){{$item->file_name}}@endisset</p>

                    @isset($item->repository_url)<a target="_blank" href="{{$item->repository_url}}">Go to
                        repository</a>@endisset
                </div>
            @endforeach

            <div class="col-xs-12 col-md-12 col-lg-12 nextPreviousContainer">
                @isset($previousLink)
                    <div class="pull-left">
                        <a href="{{url($previousLink)}}" class="btn btn-link">@if($previousLink=="") Back to home page @else Previous @endif</a>
                    </div>
                @endisset
                <div class="pull-right">
                    <a href="{{url($nextLink)}}" class="btn btn-link">Next</a>
                </div>
            </div>
        @else
            <div>
                <p>No results found, return to login page.</p>
            </div>
            <div>
                <a href="{{url('/login')}}">return to login</a>
            </div>
        @endif
    </div>
@stop()
