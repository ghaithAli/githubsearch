@extends('layouts.main')

@section('content')

    <div class="col-xs-12">
        <h2>Your results are ready</h2>
    </div>
    <div class="col-xs-12">
        @foreach($result as $item)
            <div class="col-xs-12 col-md-6 col-lg-4 well mainContainer">
                <a href="{{$reply->value}}">Here are the results</a>
            </div>
        @endforeach
    </div>
@stop()
