<!DOCTYPE html>
<html lang="en">
<head>
    <title>{{ config('app.name', 'Meet2Talk') }}</title>

    <link href="{{ url('css/app.css') }}" rel="stylesheet">
    <link href="{{ url('css/main.css') }}" rel="stylesheet">
    @yield('styles')

</head>
<body>
    @include('layouts.header')
    <div class="container">
        @yield('content')
    </div>
    @include('layouts.footer')

    <script type="text/javascript" src="{{ url('js/jQuery/jquery-3.1.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/bootstrap/bootstrap.min.js') }}"></script>
    @yield('scripts')
</body>