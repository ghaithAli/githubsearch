<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {

});

/*
 * Create a production ready simple REST API for searching in all Github code
 *
 * Requirements:
 * 1. Must be easy to replace, so I can change provider if I some day want to use GitLab or Bitbucket, or all of them.
 *
 * 2.Must have an endpoint that accepts a query, and returns the paginated result. One hit (result) must comprise of owner name, repository name and file name
 *
 * 3.The number of hits per page should be 25 by default, but must be changeable by a query string parameter
 *
 * 4.The page number should be changeable by a query string parameter
 *
 * 5.The sorting should be by score, but must be changeable by a query string parameter
 *
 * */

//Search API
Route::get('/','SearchController@presetHomeView');
Route::post('/search','SearchController@makeSearch');
Route::get('/results',['uses'=>'SearchController@presentSearchResult']);


//those routes are the OAuth routes to get the token for login to GitHub.
Route::get('/redirect', 'SocialAuthGithubController@redirectToProvider');
Route::get('/callback', 'SocialAuthGithubController@handleProviderCallback');




Route::get('/home', 'HomeController@index')->name('home');
Auth::routes();