<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Result extends Model
{

    protected $table = "github_results";

    protected $fillable = [
        'repository_name',
        'owner_name',
        'file_name',
        'repository_url',
        'provider',
        'search_text',
        'rating'
    ];

}