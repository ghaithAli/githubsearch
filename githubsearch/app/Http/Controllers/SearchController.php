<?php
/**
 * Created by PhpStorm.
 * User: ghaithali
 * Date: 2/7/18
 * Time: 12:55 PM
 */

namespace App\Http\Controllers;

use App\Model\Result;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

use App\Util\ResponseClass;
use Illuminate\Support\Facades\DB;
use Laravel\Socialite\Facades\Socialite;

class SearchController extends Controller
{
    public function presentSearchView()
    {

        return view('contents.search');
    }

    /*public function makeSearch(Request $request){
        return redirect()->route('searchResults',['search_text' => $request->search_text, 'number_of_hits' => $request->number_of_hits,'sorting_type'=>$request->sorting_type ,'result_index' => 1]);
    }*/


    public function makeSearch(Request $request)
    {
        // get the search engine we are using from the .env file.
        $searchEngine = $_ENV['SEARCH_ENGINE'];
        $result = ResponseClass::createInvalidReplyWithMessage("something went wrong");

        if ($searchEngine == 'GitHub') {
            $result = $this->makeSearchRequestToGitHubAPI($request);
        } else if ($searchEngine == 'BitBucket') {
            /*
             * here we can extend the app so that the provider can be something else like
             * BitBucket or something else.
             */

            //fx.
            $result = $this->makeSearchRequestToBitBucketAPI($request);
        }
        return $result;
    }

    public function presetHomeView()
    {

        return view('home');
    }

    private function makeSearchRequestToGitHubAPI($request)
    {
        //Here we make a call Git external API using Guzzle library
        //http://docs.guzzlephp.org/en/stable/

        /*
         * In order to search all the GitHub code base, The GitHub API requires the users to be authenticated,
         * https://developer.github.com/v3/search/#search-code
         *
         */

        $reply = ResponseClass::createInvalidReplyWithMessage("Something went wrong");

        //we are posting to the database, we we use the transaction function to pack the whole function
        //in one transaction in case of a failure.
        DB::transaction(function () use ($request, &$reply) {
            if (isset($request->code_text)) {

                //we check if we have data for the current search before creating entries in our own database:
                $testResult = Result::where('search_text', $request->code_text)->where('provider', 'github')->get();

                if (sizeof($testResult) > 0) {
                    //we did this search before, just redirect to results page.
                } else {
                    //fetch results from Git server
                    $client = new Client(['base_uri' => 'https://api.github.com/search/', 'timeout' => 12.0]);
                    $response = $client->request('GET', 'code?q=' . $request->code_text . '&access_token=' . $request->access_token);
                    if ($response->getBody()) {
                        $objectResponse = \GuzzleHttp\json_decode($contents = (string)$response->getBody());
                        $itemsResponse = $objectResponse->items;
                        //create array to return only requested information (repository, owner and file name).
                        foreach ($itemsResponse as $item) {
                            //Another approach:
                            //we can return the data we got from the GitHub API directly
                            /*
                            $newItem = array(
                                'repository_name'=> $item->repository->name,
                                "owner_name" => $item->repository->owner->login,
                                "file_name" => $item->name,
                            "repository_url" => $item->repository->html_url);
                            array_push($newArray, $newItem);
                            return $newArray;
                            */


                            //this is a new search, save the result in our own database and redirect.
                            $newItemData = [
                                'repository_name' => $item->repository->name,
                                'owner_name' => $item->repository->owner->login,
                                'file_name' => $item->name,
                                'repository_url' => $item->repository->html_url,
                                'search_text' => $request->code_text,
                                'rating' => $item->score,
                                'provider' => 'github'
                            ];
                            Result::create($newItemData);
                        }
                    } else {
                        $reply = ResponseClass::createValidReplyWithValue("");
                    }
                }

                $resultSize = $request->results_size;
                $search_text = $request->code_text;

                $orderBy = $request->sort_type;


                $reply = redirect ('/results?search_text=' . $search_text . '&sort=' . $orderBy . '&result_size=' . $resultSize . '&page=0');
            } else {
                $reply = ResponseClass::createInvalidReplyWithMessage("Please enter a text query.");
            }
        });

        return $reply;
    }

    public function presentSearchResult(Request $request)
    {
        //basic error handling, check if parameters exist
        if(isset($request->sort) && ($request->sort == "rating" || $request->sort == "owner_name" || $request->sort == "repository_name" || $request->sort == "file_name")){
            $orderBy = $request->sort;
        }else{
            //default sort by score.
            $orderBy = "rating";
        }

        if(isset($request->result_size) && is_numeric($request->result_size)){
            $resultSize = $request->result_size;
        }else{
            $resultSize = 25;
        }

        if(isset($request->page) && is_numeric($request->page)){
            $page = $request->page;
        }else{
            $page = 0;
        }

        if(isset($request->search_text) && $request->search_text !=""){
            $text_search = $request->search_text;

            //Eloquent query to get result:
            $results = Result::where('search_text', $text_search)->orderBy($orderBy)->take($resultSize)->skip($page * $resultSize)->get();

            //define next page link:
            $nextPage = $page+1;
            $nextLink = '/results?search_text=' . $text_search . '&sort=' . $orderBy . '&result_size=' . $resultSize . '&page=' . $nextPage;

            //define previous page link:
            if($page>0){
                $previousPage = $page-1;
                $previousLink = '/results?search_text=' . $text_search . '&sort=' . $orderBy . '&result_size=' . $resultSize . '&page=' . $previousPage;
            }else{
                $previousLink = "";
            }

            $data = [
                'result' => $results,
                'nextLink' => $nextLink,
                'previousLink' => $previousLink

            ];

            return view('contents.results', $data);
        }else{
            //mandatory missing
            return ResponseClass::createInvalidReplyWithMessage("Missing code query");
        }
    }

    private function makeSearchRequestToBitBucketAPI($request)
    {
        //send result back from BitBucket server.
    }
}