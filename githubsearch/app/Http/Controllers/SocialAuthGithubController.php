<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laravel\Socialite\Facades\Socialite;

class SocialAuthGithubController extends Controller
{
    /**
     * Create a redirect method to GitHub api.
     *
     * @return void
     */
    public function redirectToProvider()
    {
        return Socialite::driver('github')->redirect();
    }

    /**
     * Return a callback method from facebook api.
     *
     * @return callback URL from facebook
     */
    public function handleProviderCallback()
    {

        //$user = Socialite::driver('github')->stateless()->user();
        $user = Socialite::driver('github')->user();

        $data = [
            'user' => $user
        ];
        return view('contents.search', $data);


    }
}
