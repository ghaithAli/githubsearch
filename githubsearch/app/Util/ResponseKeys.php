<?php
/**
 * Created by PhpStorm.
 * User: ghaithali
 * Date: 3/30/18
 * Time: 4:43 PM
 */

namespace App\Util;

class ResponseKeys
{
    // list of keys used in the requests
    const ERROR_CODE_KEY = "error";
    const ERROR_MSG_KEY = "message";

    const RETURN_VALUE_KEY = "value";
}