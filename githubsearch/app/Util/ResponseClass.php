<?php
/**
 * Created by PhpStorm.
 * User: ghaithali
 * Date: 3/30/18
 * Time: 4:42 PM
 */

namespace App\Util;


class ResponseClass
{
    /**
     *	creates a valid response object with a value
     *
     *	@param object
     *	@return array, a valid reply json object in the format
     *  {"error":"OK", "value":"jsonObject"}
     */
    public static function createValidReplyWithValue($value){
        $reply = array(
            ResponseKeys::ERROR_CODE_KEY => StatusCode::STATUS_CODE_OK,
            ResponseKeys::RETURN_VALUE_KEY => $value
        );

        return json_encode($reply);
    }

    /**
     *	creates an invalid response with a message
     *
     *	@param string message to explain the error.
     *	@return array, the reply object that represents the failed operation
     */
    public static function createInvalidReplyWithMessage($message){
        $reply = array(
            ResponseKeys::ERROR_CODE_KEY => StatusCode::STATUS_CODE_ERROR,
            ResponseKeys::ERROR_MSG_KEY => $message
        );

        return json_encode($reply);
    }
}