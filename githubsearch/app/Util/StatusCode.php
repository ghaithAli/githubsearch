<?php
/**
 * Created by PhpStorm.
 * User: ghaithali
 * Date: 3/30/18
 * Time: 4:43 PM
 */

namespace App\Util;

class StatusCode{
    const STATUS_CODE_OK = "OK";
    const STATUS_CODE_ERROR = "ERROR";
    //here we can add more status codes for the different errors.
}